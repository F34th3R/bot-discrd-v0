import discord
from discord.ext import commands

from urllib import parse, request
import re

import datetime

bot = commands.Bot(command_prefix= '>', description= "This is a helper bot")

@bot.command()
async def ping(ctx):
  await ctx.send('pong')

@bot.command()
async def info(ctx):
  embed = discord.Embed(title= f"{ctx.guild.name}", description= "Lorem ipsum",
  timestamp= datetime.datetime.utcnow(), color= discord.Color.orange())
  embed.add_field(name= "Server created at", value= f"{ctx.guild.created_at}")
  embed.add_field(name= "Server Owner", value= f"{ctx.guild.owner}")
  embed.add_field(name= "Server Region", value= f"{ctx.guild.region}")
  embed.add_field(name= "Server ID", value= f"{ctx.guild.id}")
  embed.add_field(name= "...", value= "wwww.f34th3r.io")
  # embed.set_thumbnail(url= f"{ctx.guild.icon}")
  embed.set_thumbnail(url= "https://cdn.dribbble.com/users/37530/screenshots/2937858/drib_blink_bot.gif")
  await ctx.send(embed= embed)

@bot.command()
async def youtube(ctx, *, search):
  query_string = parse.urlencode({'search_query': search})
  html_content = request.urlopen('http://www.youtube.com/results?' + query_string)
  # print(html_content.read().decode())
  search_results = re.findall('href=\"\\/watch\\?v=(.{11})', html_content.read().decode())
  print(search_results)
  # I will put just the first result, you can loop the response to show more results
  await ctx.send('https://www.youtube.com/watch?v=' + search_results[0])

# Events
@bot.event
async def on_ready():
  await bot.change_presence(activity=discord.Streaming(
    name="Waiting", url="www.twitch.tv/accountname"))
  print("I'm ready! \n( ͡° ͜ʖ ͡°)")

bot.run('NTcwMTI2NjMyMzgwMjY4NTQ0.XL6qCA.HthWex34OJyYTlzvNf3VIJVa4sk')